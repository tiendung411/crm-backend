import { describe } from 'ava-spec';
import { stringify } from 'query-string';
import request from 'supertest';
import uuidv4 from 'uuid/v4';
import { getAllElements, loadFixtures } from '../helpers';
import app from '../../src/app';

const URI = '/company/2ba90f30-6975-11e8-8b90-9b391703c11a/branch';
/* eslint-disable */
const headers = {
    'x-access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50Ijp7ImlkIjoiOWYwM2VjNDctMmI5MS00YzIxLTlmNTYtN2VmOWZlM2Y5YzE3Iiwicm9sZXMiOlsiY29tcGFueV9hZG1pbiJdLCJ0ZW5hbnRfaWQiOiIyYmE5MGYzMC02OTc1LTExZTgtOGI5MC05YjM5MTcwM2MxMWEifSwic3RhZmYiOnsiaWQiOiI1NzcwM2M0Ni0zNTE2LTQxMDUtOTA1OS00NWQ0ZDhlYTI2MjkiLCJicmFuY2hfaWQiOiI5ZGRiYjhiYS1iNTI1LTQ5MzEtOTIwYS01YWY5NDFmMWIwOTMiLCJ0ZW5hbnRfaWQiOiIyYmE5MGYzMC02OTc1LTExZTgtOGI5MC05YjM5MTcwM2MxMWEifSwidG9rZW5UeXBlIjoiYWNjZXNzX3Rva2VuIiwiaWF0IjoxNTQ3MTAyMzE1fQ.73SuSYP8gDQeVAdVAJXkcBR6CDfygrF_vaJhRyQQGCs'
};

let dbObjects;
describe.serial('Branch API', it => {
    it.beforeEach(() =>
        loadFixtures()
            .then(() => getAllElements('Branch'))
            .then(response => {
                dbObjects = response;
            })
    );
    it('should retrieve a list of all companies', async t => {
        const response = await request(app)
            .get(URI)
            .set(headers)
            .expect(200)
            .then(res => res.body);
        t.is(response.count, 3);
        t.is(response.rows.length, dbObjects.length);
    });

    it('should retrieve a list of 1 branch with limit 1', async t => {
        const response = await request(app)
            .get(`${URI}?limit=1`)
            .set(headers)
            .expect(200)
            .then(res => res.body);
        t.is(response.count, 3);
        t.is(response.rows.length, 1);
        t.is(response.rows[0].id, '9ddbb8ba-b525-4931-920a-5af941f1b093');
    });

    it('should retrieve a list of 1 branch with limit 1 and offset 1', async t => {
        const response = await request(app)
            .get(`${URI}?limit=1&offset=1`)
            .set(headers)
            .expect(200)
            .then(res => res.body);
        t.is(response.count, 3);
        t.is(response.rows.length, 1);
        t.is(response.rows[0].id, '9ddbb8ba-b525-4931-920a-5af941f1b039');
    });

    it('should retrieve a list of 1 branch with limit 1 and offset 0', async t => {
        const response = await request(app)
            .get(`${URI}?limit=1&offset=0`)
            .set(headers)
            .expect(200)
            .then(res => res.body);
        t.is(response.count, 3);
        t.is(response.rows.length, 1);
        t.is(response.rows[0].id, '9ddbb8ba-b525-4931-920a-5af941f1b093');
    });

    it('should return ResourceNotFound when retrieving nonexisting branch', async t => {
        const response = await request(app)
            .get(`${URI}/${uuidv4()}`)
            .set(headers)
            .expect(404)
            .then(res => res.body);
        t.is(response.name, 'ResourceNotFoundError');
        t.is(response.message, 'Could not find Branch');
    });

    it('should return a branch', async t => {
        const fixture = dbObjects[0];
        const response = await request(app)
            .get(`${URI}/${fixture.id}`)
            .set(headers)
            .expect(200)
            .then(res => res.body);
        t.is(JSON.stringify(response), JSON.stringify(fixture));
        t.is(response.code, fixture.code);
    });

    it('can search branch by name', async t => {
        const keyword = 'branch';
        const response = await request(app)
            .get(`${URI}?search=${keyword}`)
            .set(headers)
            .expect(200)
            .then(res => res.body);
        let result = false;
        let count = response.count;
        if (count >= 1) {
            result = true;
        }
        t.is(result, true);
    });

    it('can search branch by type', async t => {
        const keyword = 'Head Quarter';
        const response = await request(app)
            .get(`${URI}?search=${keyword}`)
            .set(headers)
            .expect(200)
            .then(res => res.body);
        let result = false;
        let count = response.count;
        if (count >= 1) {
            result = true;
        }
        t.is(result, true);
    });

    it('should add a new branch', async t => {
        const today = `${(new Date()).getTime()}`;
        const content = {
            name: `Branch${today}`,
            phone: `${today.substr(6)}`,
            tax_code: `000${today}`,
            address: {
                "ward_code": "03203",
                "region_code": "11",
                "district_code": "101",
                "street_address": "152 Nguyễn Trãi"
            },
            is_main: true
        };
        const response = await request(app)
            .post(URI)
            .set(headers)
            .send(content)
            .expect(200)
            .then(res => res.body);
        t.is(response.name, content.name);
        t.is(response.phone, content.phone);
        t.is(response.tax_code, content.tax_code);
        t.is(response.address.ward_code, content.address.ward_code);
        t.is(response.address.region_code, content.address.region_code);
        t.is(response.address.district_code, content.address.district_code);
        t.is(response.address.street_address, content.address.street_address);
        t.is(response.is_main, content.is_main);
    });

    it('should not add a new branch with missing field', async t => {
        const today = `${(new Date()).getTime()}`;
        const content = {
            name: null,
            tax_code: null,
            address: null,
            phone: null,
            is_main: null,
            company_id: '9ddbb8ba-b525-4931-920a-5af941f1b038',

        };
        const response = await request(app)
            .post(URI)
            .set(headers)
            .send(content)
            .expect(400)
            .then(res => res.body);
        t.is(response.name, 'RequestBodyValidationError');
        t.is(response.payload.name.type, 'string.base');
        t.is(response.payload.name.message, `"name" must be a string`);
        t.is(response.payload.tax_code.type, 'string.base');
        t.is(response.payload.tax_code.message, `"tax_code" must be a string`);
        t.is(response.payload.address.type, 'object.base');
        t.is(response.payload.address.message, `"address" must be an object`);
        t.is(response.payload.phone.type, 'string.base');
        t.is(response.payload.phone.message, `"phone" must be a string`);
    });

    it('should update branch', async t => {
        const fixture = dbObjects[0];
        const today = `${(new Date()).getTime()}`;
        const content = {
            name: `Branch${today}`,
            phone: `${today.substr(6)}`,
            tax_code: `000${today}`,
            address: {
                "ward_code": "03203",
                "region_code": "11",
                "district_code": "101",
                "street_address": "152 Nguyễn Trãi"
            },
            is_main: false
        };
        const response = await request(app)
            .put(`${URI}/${fixture.id}`)
            .set(headers)
            .send(content)
            .expect(200)
            .then(res => res.body);
        t.is(response.name, content.name);
        t.is(response.tax_code, content.tax_code);
        t.is(response.address.ward_code, content.address.ward_code);
        t.is(response.address.region_code, content.address.region_code);
        t.is(response.address.district_code, content.address.district_code);
        t.is(response.address.street_address, content.address.street_address);
        t.is(response.phone, content.phone);
        t.is(response.is_main, content.is_main);
    });

    it('should not update branch with empty fields', async t => {
        const fixture = dbObjects[0];
        const content = {
            name: null,
            tax_code: null,
            address: null,
            phone: null,
            is_main: null
        };
        const response = await request(app)
            .put(`${URI}/${fixture.id}`)
            .set(headers)
            .send(content)
            .expect(400)
            .then(res => res.body);
        t.is(response.name, 'RequestBodyValidationError');
        t.is(response.payload.name.type, 'string.base');
        t.is(response.payload.name.message, `"name" must be a string`);
        t.is(response.payload.tax_code.type, 'string.base');
        t.is(response.payload.tax_code.message, `"tax_code" must be a string`);
        t.is(response.payload.address.type, 'object.base');
        t.is(response.payload.address.message, `"address" must be an object`);
        t.is(response.payload.phone.type, 'string.base');
        t.is(response.payload.phone.message, `"phone" must be a string`);
    });

    it('can search branch by tax code', async t => {
        const keyword = 123456;
        const response = await request(app)
            .get(`${URI}?search=${keyword}`)
            .set(headers)
            .expect(200)
            .then(res => res.body);
        let result = false;
        let count = response.count;
        if (count >= 1) {
            result = true;
        }
        t.is(result, true);
    });

    it('can search branch by address', async t => {
        const search = 'Điện Biên Phủ';
        const response = await request(app)
            .get(`${URI}?${stringify({ search })}`)
            .set(headers)
            .expect(200)
            .then(res => res.body);
        let result = false;
        let count = response.count;
        if (count >= 1) {
            result = true;
        }
        t.is(result, true);
    });
});
