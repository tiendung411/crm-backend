import { describe } from 'ava-spec';
import { stringify } from 'query-string';
import request from 'supertest';
import uuidv4 from 'uuid/v4';
import { getAllElements, loadFixtures } from '../helpers';
import app from '../../src/app';

const URI = '/company';
const URI_2 = '/company/account';
/* eslint-disable */
const headers = {
    'x-access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50Ijp7ImlkIjoiMDM3MWFmZjEtY2MzMS00OWFhLWFlNDgtY2RlY2Y3NGQ3OWJkIiwicm9sZXMiOlsic3VwZXJfYWRtaW4iXSwidGVuYW50X2lkIjoiOWRkYmI4YmEtYjUyNS00OTMxLTkyMGEtNWFmOTQxZjFiMDM4In0sInRva2VuVHlwZSI6ImFjY2Vzc190b2tlbiIsImlhdCI6MTU0NzUxOTI0MH0.XMCylRlpaT88BPC-4OKtV-jq3QG1RqNrdKN4pfBjk0g'
};

let dbObjects;
describe.serial('Company API', it => {
    it.beforeEach(() =>
        loadFixtures()
            .then(() => getAllElements('Company'))
            .then(response => {
                dbObjects = response;
            })
    );
    it('should retrieve a list of all companies', async t => {
        const response = await request(app)
            .get(URI)
            .set(headers)
            .expect(200)
            .then(res => res.body);
        t.is(response.count, 3);
        t.is(response.rows.length, dbObjects.length);
    });

    it('should retrieve a list of 1 company with limit 1', async t => {
        const response = await request(app)
            .get(`${URI}?limit=1`)
            .set(headers)
            .expect(200)
            .then(res => res.body);
        t.is(response.count, 3);
        t.is(response.rows.length, 1);
        t.is(response.rows[0].id, '67bfc7e0-bce3-11e8-ad9b-ab2c21cbbfa0');
    });

    it('should retrieve a list of 1 company with limit 1 and offset 1', async t => {
        const response = await request(app)
            .get(`${URI}?limit=1&offset=1`)
            .set(headers)
            .expect(200)
            .then(res => res.body);
        t.is(response.count, 3);
        t.is(response.rows.length, 1);
        t.is(response.rows[0].id, '2ba90f30-6975-11e8-8b90-9b391703c11a');
    });

    it('should retrieve a list of 1 company with limit 1 and offset 0', async t => {
        const response = await request(app)
            .get(`${URI}?limit=1&offset=0`)
            .set(headers)
            .expect(200)
            .then(res => res.body);
        t.is(response.count, 3);
        t.is(response.rows.length, 1);
        t.is(response.rows[0].id, '67bfc7e0-bce3-11e8-ad9b-ab2c21cbbfa0');
    });

    it('should return ResourceNotFound when retrieving nonexisting company', async t => {
        const response = await request(app)
            .get(`${URI}/${uuidv4()}`)
            .set(headers)
            .expect(404)
            .then(res => res.body);
        t.is(response.name, 'ResourceNotFoundError');
        t.is(response.message, 'Could not find Company');
    });

    it('should return a company', async t => {
        const fixture = dbObjects[0];
        const response = await request(app)
            .get(`${URI}/${fixture.id}`)
            .set(headers)
            .expect(200)
            .then(res => res.body);
        t.is(JSON.stringify(response), JSON.stringify(fixture));
        t.is(response.code, fixture.code);
    });

    it('can search company by name', async t => {
        const keyword = 'long';
        const response = await request(app)
            .get(`${URI}?search=${keyword}`)
            .set(headers)
            .expect(200)
            .then(res => res.body);
        let result = false;
        let count = response.count;
        if (count >= 1) {
            result = true;
        }
        t.is(result, true);
    });

    it('should add a new company', async t => {
        const today = `${(new Date()).getTime()}`;
        const content = {
            name: `Company${today}`,
            tax_code: `000${today}`,
            "address": {
                "ward_code": "03203",
                "region_code": "11",
                "district_code": "101",
                "street_address": "152 Nguyễn Trãi"
            },
            settings: {
                defaultLanguage: "en-US"
            }
        };
        const response = await request(app)
            .post(URI)
            .set(headers)
            .send(content)
            .expect(200)
            .then(res => res.body);
        t.is(response.name, content.name);
        t.is(response.tax_code, content.tax_code);
        t.is(response.address.ward_code, content.address.ward_code);
        t.is(response.address.region_code, content.address.region_code);
        t.is(response.address.district_code, content.address.district_code);
        t.is(response.address.street_address, content.address.street_address);
        t.is(response.settings.defaultLanguage, content.settings.defaultLanguage);
    });

    it('should not add a new company because of missing required field', async t => {
        const content = {};
        const response = await request(app)
            .post(URI)
            .set(headers)
            .send(content)
            .expect(400)
            .then(res => res.body);
        t.is(response.name, 'RequestBodyValidationError');
        t.is(response.payload.name.type, 'any.required');
        t.is(response.payload.name.message, `"name" is required`);
        t.is(response.payload.tax_code.type, 'any.required');
        t.is(response.payload.tax_code.message, `"tax_code" is required`);
        t.is(response.payload.address.type, 'any.required');
        t.is(response.payload.address.message, `"address" is required`);
    });

    it('should update information of company successfull', async t => {
        const fixture = dbObjects[0];
        const today = `${(new Date()).getTime()}`;
        const content = {
            name: `Company${today}`,
            tax_code: `000${today}`,
            "address": {
                "ward_code": "03203",
                "region_code": "11",
                "district_code": "101",
                "street_address": "152 Nguyễn Trãi"
            },
            settings: {
                defaultLanguage: "en-US",
                fileCreator: {
                    api_key: '',
                    serect_key: '',
                }
            }
        };
        const response = await request(app)
            .put(`${URI}/${fixture.id}`)
            .set(headers)
            .send(content)
            .expect(200)
            .then(res => res.body);
        t.is(response.name, content.name);
        t.is(response.tax_code, content.tax_code);
        t.is(response.address.ward_code, content.address.ward_code);
        t.is(response.address.region_code, content.address.region_code);
        t.is(response.address.district_code, content.address.district_code);
        t.is(response.address.street_address, content.address.street_address);
        t.is(response.settings.defaultLanguage, content.settings.defaultLanguage);
    });

    it('should not update company with empty fields', async t => {
        const fixture = dbObjects[0];
        const content = {
            name: null,
            tax_code: null,
            address: null
        };
        const response = await request(app)
            .put(`${URI}/${fixture.id}`)
            .set(headers)
            .send(content)
            .expect(400)
            .then(res => res.body);
        t.is(response.name, 'RequestBodyValidationError');
        t.is(response.payload.name.type, 'string.base');
        t.is(response.payload.name.message, `"name" must be a string`);
        t.is(response.payload.tax_code.type, 'string.base');
        t.is(response.payload.tax_code.message, `"tax_code" must be a string`);
        t.is(response.payload.address.type, 'object.base');
        t.is(response.payload.address.message, `"address" must be an object`);
    });

    it('should update FILE_CREATOR of company successfull', async t => {
        const fixture = dbObjects[0];
        const today = `${(new Date()).getTime()}`;
        const content = {
            settings: {
                defaultLanguage: "vi-VN",
                fileCreator: {
                    api_key: `api_key${today}`,
                    serect_key: `secrect_key${today}`,
                }
            }
        };
        const response = await request(app)
            .put(`${URI}/${fixture.id}`)
            .set(headers)
            .send(content)
            .expect(200)
            .then(res => res.body);
        t.is(response.settings.defaultLanguage, content.settings.defaultLanguage);
        t.is(response.settings.fileCreator.api_key, content.settings.fileCreator.api_key);
        t.is(response.settings.fileCreator.serect_key, content.settings.fileCreator.serect_key);
    });


    it('can search company by tax code', async t => {
        const keyword = 123456;
        const response = await request(app)
            .get(`${URI}?search=${keyword}`)
            .set(headers)
            .expect(200)
            .then(res => res.body);
        let result = false;
        let count = response.count;
        if (count >= 1) {
            result = true;
        }
        t.is(result, true);
    });

    it('Can add a company_admin account', async (t) => {
        const companyAdminAccount = {
            companyId: "2ba90f30-6975-11e8-8b90-9b391703c11a",
            password: "savemoney123",
            roles: "company_admin",
            tenantId: "2ba90f30-6975-11e8-8b90-9b391703c11a",
            username: "test_company_admin@gmail.com"
        }
        const response = await request(app)
            .post(URI_2)
            .set(headers)
            .send(companyAdminAccount)
            .expect(200)
            .then(res => res.body);
        t.is(true, true);
    });

    it('can search company by address', async t => {
        const search = 'Điện Biên Phủ';
        const response = await request(app)
            .get(`${URI}?${stringify({ search })}`)
            .set(headers)
            .expect(200)
            .then(res => res.body);
        let result = false;
        let count = response.count;
        if (count >= 1) {
            result = true;
        }
        t.is(result, true);
    });
});
