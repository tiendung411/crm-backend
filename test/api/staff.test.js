import { describe } from 'ava-spec';
import request from 'supertest';
import uuidv4 from 'uuid/v4';
import { getAllElements, loadFixtures } from '../helpers';
import app from '../../src/app';

const URI = '/staff';
/* eslint-disable */
const headers = {
    'x-access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50Ijp7ImlkIjoiOWYwM2VjNDctMmI5MS00YzIxLTlmNTYtN2VmOWZlM2Y5YzE3Iiwicm9sZXMiOlsiY29tcGFueV9hZG1pbiJdLCJ0ZW5hbnRfaWQiOiIyYmE5MGYzMC02OTc1LTExZTgtOGI5MC05YjM5MTcwM2MxMWEifSwic3RhZmYiOnsiaWQiOiI1NzcwM2M0Ni0zNTE2LTQxMDUtOTA1OS00NWQ0ZDhlYTI2MjkiLCJicmFuY2hfaWQiOiI5ZGRiYjhiYS1iNTI1LTQ5MzEtOTIwYS01YWY5NDFmMWIwOTMiLCJ0ZW5hbnRfaWQiOiIyYmE5MGYzMC02OTc1LTExZTgtOGI5MC05YjM5MTcwM2MxMWEifSwidG9rZW5UeXBlIjoiYWNjZXNzX3Rva2VuIiwiaWF0IjoxNTQ3MTAyMzE1fQ.73SuSYP8gDQeVAdVAJXkcBR6CDfygrF_vaJhRyQQGCs'
};
const tenant_id = '2ba90f30-6975-11e8-8b90-9b391703c11a';

const URI_2 = '/company/67bfc7e0-bce3-11e8-ad9b-ab2c21cbbfa0/branch';

const URI_3 = '/staff/account';

const headers_2 = {
    'x-access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50Ijp7ImlkIjoiOWYwM2VjNDctMmI5MS00YzIxLTlmNTYtN2VmOWZlM2Y5YzE3Iiwicm9sZXMiOlsiY29tcGFueV9hZG1pbiJdLCJ0ZW5hbnRfaWQiOiI5ZGRiYjhiYS1iNTI1LTQ5MzEtOTIwYS01YWY5NDFmMWIwMzgifSwidG9rZW5UeXBlIjoiYWNjZXNzX3Rva2VuIiwiaWF0IjoxNTQ3NTE4NzU0fQ.uE5Gk3cRLf4aXPN8Mmmkyp30b-o2VBPqfAyW9luzyZ0'
};
const companysaleId = '3203d330-c26f-11e8-8dda-9dd849be0fa3';

let dbObjects;
let companySale;
describe.serial('Staff API', it => {
    it.beforeEach(() =>
        loadFixtures()
            .then(() => getAllElements('Staff'))
            .then(response => {
                dbObjects = response;
            })
    );
    it('should retrieve a list of all staffs', async t => {
        const response = await request(app)
            .get(URI)
            .set(headers)
            .expect(200)
            .then(res => res.body);
        t.is(response.count, 8);
        t.is(response.rows.length, 8);
    });

    it('should retrieve a list of 1 staff with limit 1', async t => {
        const response = await request(app)
            .get(`${URI}?limit=1`)
            .set(headers)
            .expect(200)
            .then(res => res.body);
        t.is(response.count, 8);
        t.is(response.rows.length, 1);
        t.is(response.rows[0].id, 'a57ea48a-4102-4792-87b0-cee525a73288');
    });

    it('should retrieve a list of 1 staff with limit 1 has tenant_id', async t => {
        const response = await request(app)
            .get(`${URI}?limit=1`)
            .set(headers)
            .expect(200)
            .then(res => res.body);
        t.is(response.count, 8);
        t.is(response.rows.length, 1);
        t.is(response.rows[0].id, 'a57ea48a-4102-4792-87b0-cee525a73288');
        t.is(response.rows[0].tenant_id, tenant_id)
    });

    it('should retrieve a list of 1 staff with limit 1 and offset 1', async t => {
        const response = await request(app)
            .get(`${URI}?limit=1&offset=1`)
            .set(headers)
            .expect(200)
            .then(res => res.body);
        t.is(response.count, 8);
        t.is(response.rows.length, 1);
        t.is(response.rows[0].id, '9ddbb8ba-b525-4931-920a-5af941f1b083');
    });

    it('should retrieve a list staff has tenant_id', async t => {
        let flag;
        const response = await request(app)
            .get(`${URI}`)
            .set(headers)
            .expect(200)
            .then(res => res.body);
        t.is(response.count, 8);
        for (let i = 0; i < response.count; i++) {
            if (response.rows[i].tenant_id == tenant_id) {
                flag = true;
            } else {
                flag = false;
                break;
            }
        }
        t.is(flag, true);
    });

    it('should retrieve a list of 1 staff with limit 1 and offset 1 has tenant_id', async t => {
        const response = await request(app)
            .get(`${URI}?limit=1&offset=1`)
            .set(headers)
            .expect(200)
            .then(res => res.body);
        t.is(response.count, 8);
        t.is(response.rows.length, 1);
        t.is(response.rows[0].id, '9ddbb8ba-b525-4931-920a-5af941f1b083');
        t.is(response.rows[0].tenant_id, tenant_id);
    });

    it('should return ResourceNotFound when retrieving nonexisting staff', async t => {
        const response = await request(app)
            .get(`${URI}/${uuidv4()}`)
            .set(headers)
            .expect(404)
            .then(res => res.body);
        t.is(response.name, 'ResourceNotFoundError');
        t.is(response.message, 'Could not find Staff');
    });


    it('Can add a staff account', async (t) => {
        const staffAccount = {
            staffId: 'a57ea48a-4102-4792-87b0-cee525a73288',
            username: 'test_account@gmail.com',
            password: 'savemoney123',
            tenantId: '2ba90f30-6975-11e8-8b90-9b391703c11a',
            roles: 'uw_staff'
        };
        await request(app)
            .post(URI_3)
            .set(headers)
            .send(staffAccount)
            .expect(200)
            .then(res => res.body);
        t.is(true, true);
    })

    it('should return a staff', async t => {
        const fixture = dbObjects[0];
        const response = await request(app)
            .get(`${URI}/${fixture.id}`)
            .set(headers)
            .expect(200)
            .then(res => res.body);
        t.is(response.code, fixture.code);
    });

    it('can search by name', async t => {
        const keyword = 'Khanh';
        const response = await request(app)
            .get(`${URI}?search=${keyword}`)
            .set(headers)
            .expect(200)
            .then(res => res.body);
        t.is(response.count, 1);
        t.is(response.rows.length, 1);
        let result = false;
        if (response.rows[0].name.indexOf(keyword) > -1) {
            result = true;
        }
        t.is(result, true);
    });

    it('can search by name successfull response has tenant_id', async t => {
        const keyword = 'Khanh';
        const response = await request(app)
            .get(`${URI}?search=${keyword}`)
            .set(headers)
            .expect(200)
            .then(res => res.body);
        t.is(response.count, 1);
        t.is(response.rows.length, 1);
        t.is(response.rows[0].tenant_id, tenant_id)
        let result = false;
        if (response.rows[0].name.indexOf(keyword) > -1) {
            result = true;
        }
        t.is(result, true);
    });

    it('can search by name of staff not match company', async t => {
        const today = `${(new Date()).getTime()}`;
        const contentBranch = {
            name: `Branch${today}`,
            phone: `${today.substr(6)}`,
            tax_code: `000${today}`,
            address: {
                "ward_code": "03203",
                "region_code": "11",
                "district_code": "101",
                "street_address": "152 Nguyễn Trãi"
            },
            is_main: true
        };
        const responseBranch = await request(app)
            .post(URI_2)
            .set(headers_2)
            .send(contentBranch)
            .expect(200)
            .then(res => res.body);

        const contentStaff = {
            "name": "Trần Khanh",
            "dob": "1995-10-20",
            "phone": '123456789',
            "address": {
                "ward_code": "03203",
                "region_code": "11",
                "district_code": "101",
                "street_address": "152 Nguyễn Trãi"
            },
            "national_id": "241566099",
            "branch_id": responseBranch.branch_id,
            "account_id": null
        };
        const responseStaff = await request(app)
            .post(URI)
            .set(headers_2)
            .send(contentStaff)
            .expect(200)
            .then(res => res.body);
        const response = await request(app)
            .get(`${URI}/${responseStaff.id}`)
            .set(headers)
            .expect(404)
            .then(res => res.body);
        t.is(response.message, 'Could not find Staff');
    });

    it('should add a new staff successfull', async t => {
        const content = {
            "name": "Trần Khanh",
            "dob": "1995-10-20",
            "phone": null,
            address: {
                "ward_code": "03203",
                "region_code": "11",
                "district_code": "101",
                "street_address": "152 Nguyễn Trãi"
            },
            "national_id": "241566099",
            "branch_id": "9ddbb8ba-b525-4931-920a-5af941f1b038",
            "account_id": null
        };
        const response = await request(app)
            .post(URI)
            .set(headers)
            .send(content)
            .expect(200)
            .then(res => res.body);
        t.is(response.name, content.name);
        t.is(response.dob, content.dob);
        t.is(response.phone, content.phone);
        t.is(response.address.ward_code, content.address.ward_code);
        t.is(response.address.region_code, content.address.region_code);
        t.is(response.address.district_code, content.address.district_code);
        t.is(response.address.street_address, content.address.street_address);
        t.is(response.national_id, content.national_id);
        t.is(response.branch_id, content.branch_id);
        t.is(response.account_id, content.account_id);
    });

    it('should add a new staff successfull response has tenant_id', async t => {
        const content = {
            "name": "Trần Khanh",
            "dob": "1995-10-20",
            "phone": null,
            address: {
                "ward_code": "03203",
                "region_code": "11",
                "district_code": "101",
                "street_address": "152 Nguyễn Trãi"
            },
            "national_id": "241566099",
            "branch_id": "9ddbb8ba-b525-4931-920a-5af941f1b038",
            "account_id": null
        };
        const response = await request(app)
            .post(URI)
            .set(headers)
            .send(content)
            .expect(200)
            .then(res => res.body);
        t.is(response.name, content.name);
        t.is(response.dob, content.dob);
        t.is(response.phone, content.phone);
        t.is(response.address.ward_code, content.address.ward_code);
        t.is(response.address.region_code, content.address.region_code);
        t.is(response.address.district_code, content.address.district_code);
        t.is(response.address.street_address, content.address.street_address);
        t.is(response.national_id, content.national_id);
        t.is(response.branch_id, content.branch_id);
        t.is(response.tenant_id, tenant_id);
    });

    it('should updated staff with id', async t => {
        const fixture = dbObjects[0];
        const content = {
            "name": "Trần Khanh Update",
            "dob": "1995-10-20",
            "phone": "123456789",
            "address": {
                "ward_code": "03203",
                "region_code": "11",
                "district_code": "101",
                "street_address": "152 Nguyễn Trãi"
            },
            "national_id": "241566099",
            "branch_id": "9ddbb8ba-b525-4931-920a-5af941f1b038",
            "account_id": null
        };
        const response = await request(app)
            .put(`${URI}/${fixture.id}`)
            .set(headers)
            .send(content)
            .expect(200)
            .then(res => res.body);
        t.is(response.name, content.name);
        t.is(response.dob, content.dob);
        t.is(response.phone, content.phone);
        t.is(response.address.ward_code, content.address.ward_code);
        t.is(response.address.region_code, content.address.region_code);
        t.is(response.address.district_code, content.address.district_code);
        t.is(response.address.street_address, content.address.street_address);
        t.is(response.national_id, content.national_id);
        t.is(response.branch_id, content.branch_id);
        t.is(response.account_id, content.account_id);
    });

    it('should not add a new staff because of missing required field', async t => {
        const content = {
            "name": "Trần Khanh",
            "dob": null,
            "phone": null,
            "address": {
                "ward_code": "03203",
                "region_code": "11",
                "district_code": "101",
                "street_address": "152 Nguyễn Trãi"
            },
            "national_id": "241566099",
            "account_id": null
        };
        const response = await request(app)
            .post(URI)
            .set(headers)
            .send(content)
            .expect(400)
            .then(res => res.body);
        t.is(response.name, 'RequestBodyValidationError');
        t.is(response.payload.dob.type, 'date.base');
        t.is(response.payload.dob.message, `"dob" must be a number of milliseconds or valid date string`);
    });

    it('should updated staff with username', async t => {
        const fixture = dbObjects[0];
        const content = {
            "name": "Trần Khanh Update",
            "dob": "1995-10-20",
            "phone": null,
            address: {
                "ward_code": "03203",
                "region_code": "11",
                "district_code": "101",
                "street_address": "152 Nguyễn Trãi"
            },
            "national_id": "241566099",
            "branch_id": "9ddbb8ba-b525-4931-920a-5af941f1b038",
            "account_id": null
        };
        const response = await request(app)
            .put(`${URI}/${fixture.username}`)
            .set(headers)
            .send(content)
            .expect(200)
            .then(res => res.body);
        t.is(response.name, content.name);
        t.is(response.dob, content.dob);
        t.is(response.phone, content.phone);
        t.is(response.address.ward_code, content.address.ward_code);
        t.is(response.address.region_code, content.address.region_code);
        t.is(response.address.district_code, content.address.district_code);
        t.is(response.address.street_address, content.address.street_address);
        t.is(response.national_id, content.national_id);
        t.is(response.branch_id, content.branch_id);
        t.is(response.account_id, content.account_id);
        t.is(response.tenant_id, tenant_id);
    });

    it('Can delete a staff', async (t) => {
        const staff = dbObjects[1];
        const response = await request(app)
            .delete(`${URI}/${staff.id}`)
            .set(headers)
            .expect(200)
            .then(res => res.body);
        t.is(response.count, 1);
    });
})
