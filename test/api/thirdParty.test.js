import { describe } from 'ava-spec';
import crypto from 'crypto';
import request from 'supertest';
import { modules } from 'firis-service-sdk-test2';
import app from '../../src/app';
import { getAllElements, loadFixtures } from '../helpers';
import config from '../../src/config';

const redis = modules.redis({
    port: config.redisPort,
    host: config.redisHost,
    password: config.redisPass
});

const URI = '/api/staff';
const DATA = {
    account: {
        id: '9ddbb8ba-b525-4931-920a-5af941f1b012',
        roles: ['company_admin'],
        tenant_id: '2ba90f30-6975-11e8-8b90-9b391703c11a'
    },
    "publicKey": "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAthdL4uXk7IyUk+HMV8Fx\n6g2IQrtWqCB0d3tzm3uSWQJkyngeK3jWEpvNLKpu7Or1O/mUiZjV4Jz9bNciG65J\nJBPXYza5XXXztbK0cH+Q886f2W+LYSBnVS/nhjIFDGyXaOFO64jZs9D0uteedZGu\nFGf0ZjftJ6WDyBaCXgqld+u47+OsTIecDgy4wn1c/z197EFiyqFiZhpajo3kz4f9\nED2+VH3rSGaAlvWhkP1KQZ0OLMYx0Nurpxf7xyrFluMICL+02cL3N0jMcYqH9opU\noeiqV2Rjngl80O6PznE8Yr8EGQwprY1mETV2u11QkOwa9YhLIpxymVnLx9uvA6kU\nCQIDAQAB\n-----END PUBLIC KEY-----" // eslint-disable-line
};
const redisConstantName = '9lkwi7hj-b587-5482-920a-5af941f1b012';
const headers = {
    api_key: redisConstantName
};

const ALGORITHM = 'aes-256-cbc';
const PRIVATE_KEY = `-----BEGIN PRIVATE KEY-----
MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC2F0vi5eTsjJST
4cxXwXHqDYhCu1aoIHR3e3Obe5JZAmTKeB4reNYSm80sqm7s6vU7+ZSJmNXgnP1s
1yIbrkkkE9djNrlddfO1srRwf5Dzzp/Zb4thIGdVL+eGMgUMbJdo4U7riNmz0PS6
1551ka4UZ/RmN+0npYPIFoJeCqV367jv46xMh5wODLjCfVz/PX3sQWLKoWJmGlqO
jeTPh/0QPb5UfetIZoCW9aGQ/UpBnQ4sxjHQ26unF/vHKsWW4wgIv7TZwvc3SMxx
iof2ilSh6KpXZGOeCXzQ7o/OcTxivwQZDCmtjWYRNXa7XVCQ7Br1iEsinHKZWcvH
268DqRQJAgMBAAECggEBAKGLXXm49zWdKptf8UfRiNxakQ6S4ThbQrhVv6SrJpTs
+LpfgnSSyWjM7GO5+Jsbf+FrdIn2G5kf7f1Hhy0DnEPzjlyoyh7ObB5AorBDnDeQ
kmP2/7J1avc6fq6hZbU2CcW0Ta3MRUewE85HSH+JdOUJrNuatj08LAOxZItF6rLN
4H4/pB0nBmHyGQ1MjPT8H44Pp1khfOuQhbrrDclGxYmEvxhOThU3jhAaj03AeoP8
32jewhPLzS9QM2QIi8bFv/2NUnH29f3/Rm2dCmCI8LYy9iYHapVxRTJbvLcd+NRg
rOQw2RchzmzLZEjAbXCaqgDfGn5PqVf4L32XgbwYK+ECgYEA31sxC7E2Zl5Q/laf
JwmXOM1qjARUwk9Qxyrt+UDTmuXblrZi0HAb3ndhJKYNew6sZroj3bsxlza5eclJ
lepC6E5wyquvIj184d0YtLh6AvpvxBAapuaaX1s4CCuPxrQKB00JzjPZvEkBa23R
ZvtwAUCOEXqVDAI53uw8mompDK0CgYEA0LQtwGtdB4niBW8KOOBcTklm+ETvYwzA
QNXKl08I2yi6BMI7GG8j7xks4Pzv7nVU1YfmYJPzvypPph8vLppdJpc6Y09d6s98
n/9p7RuMEIdSqYe2/+lbJiTp4cDcuOf3SFBG4VO8LjsYqVXSMUcuXfAmKUndidVr
rVLJ+iO11E0CgYB4078jgRlZpXoRLNDp8FFDJDSYTBuCBiA/riSQaxZNvDzfOgkY
kx0waso9ZirywAxZgia243Y/RBZwIIjTNag+PXjc2qViU3sEQHQGB6nRhKMDoGaa
lTM7lIAqQ06DApBORMmiKACIz8GZ2Q2ntStxMffo0rRgi6+8vXVHcGLn/QKBgBNA
G1w9SvZwDG4N4PaAE2ORN7M8wIv1WCJi1e4A90MnzYi01biaP8lFHDOggQ0Qu4N6
ppBP9VKzt2cOR07YJDF0D4mrf3EyND+Z0o5xVQkNQl2qzAFs5+Br1TrijDluBAwi
g3czPw9QmiS8asfq1ecKSKrUSl/1VJpHIaQ1+QadAoGBAIFMbgm37MHr16wyP6hp
6bCW/1eF3MHqBmCeotsSoVbDFmLmm9HRfjLRc0Ue8vxMpYsfZa7ct2QFIuj0kA23
r9mFXKA+ivj1bsQT9bom2QwQA9u/YeZChPlvuF/+Nfq5+UlKfuM/esYU5gsxNvl5
EsbwdP1Zi6LdKofotkv1N62+
-----END PRIVATE KEY-----`;

function decryptKeyWithRsaPrivateKey(encryptedKey, privateKey) {
    const buffer = Buffer.from(encryptedKey, 'base64');
    const decrypted = crypto.privateDecrypt(
        {
            key: privateKey,
            padding: crypto.constants.RSA_PKCS1_PADDING
        }, buffer);
    return decrypted.toString('utf8');
}

function decryptData(encryptedData, symmetricKey, iv) {
    const decipher = crypto.createDecipheriv(ALGORITHM, symmetricKey, iv);
    let decrypted = decipher.update(encryptedData, 'base64', 'utf8');
    decrypted += decipher.final('utf8');
    return decrypted;
}

function encryptData(algorithm, key, iv, data) {
    const cipher = crypto.createCipheriv(algorithm, key, iv);
    let encrypted = cipher.update(data, 'utf8', 'base64');
    encrypted += cipher.final('base64');
    return encrypted;
}
function encryptKeyWithRsaPrivateKey(privateKey, symmetricKey) {
    const buffer = Buffer.from(symmetricKey, 'utf8');
    const encrypted = crypto.privateEncrypt(privateKey, buffer);
    return encrypted.toString('base64');
}

const symmetricKey = crypto.randomBytes(32).toString('hex').slice(0, 32);
const iv = crypto.randomBytes(16).toString('hex').slice(0, 16);

describe.serial('Third Party', it => {
    it.beforeEach(() =>
        loadFixtures()
            .then(() => getAllElements('Staff'))
    );
    it('should retrieve a list of all staffs with valid API_KEY', async t => {
        await redis.set(redisConstantName, JSON.stringify(DATA));
        const response = await request(app)
            .get(URI)
            .set(headers)
            .expect(200)
            .then(res => res.body);
        const { key, iv: intialVector } = JSON.parse(decryptKeyWithRsaPrivateKey(
            response.encryptedKey, PRIVATE_KEY));
        const decryptedData = JSON.parse(decryptData(response.encryptedData, key, intialVector));
        t.is(decryptedData.count, 8);
        t.is(decryptedData.rows.length, 8);
        await redis.del(redisConstantName);
    });
    it('can not retrieve a list of all staffs with valid API_KEY with invalid API_KEY', async t => {
        const invalidHeaders = { api_key: 'api_key_does_not_exist' };
        const response = await request(app)
            .get(URI)
            .set(invalidHeaders)
            .expect(401)
            .then(res => res.body);
        t.is(response.name, 'AuthenticationError');
    });
    it('can access with valid encrypted Data and encryptedKey', async t => {
        await redis.set(redisConstantName, JSON.stringify(DATA));
        const content = {
            name: 'Trần Khanh',
            dob: '1995-10-20',
            phone: null,
            address: {
                ward_code: '03203',
                region_code: '11',
                district_code: '101',
                street_address: '152 Nguyễn Trãi'
            },
            national_id: '241566099',
            branch_id: '9ddbb8ba-b525-4931-920a-5af941f1b038',
            account_id: null
        };
        const encryptedData = encryptData(ALGORITHM, symmetricKey, iv,
            JSON.stringify(content));
        const requestData = {
            encryptedData,
            encryptedKey: encryptKeyWithRsaPrivateKey(PRIVATE_KEY,
                JSON.stringify({ key: symmetricKey, iv }))
        };
        const response = await request(app)
            .post(URI)
            .set(headers)
            .send(requestData)
            .expect(200)
            .then(res => res.body);
        const { key, iv: intialVector } = JSON.parse(decryptKeyWithRsaPrivateKey(
            response.encryptedKey, PRIVATE_KEY));
        const decryptedData = JSON.parse(decryptData(response.encryptedData, key, intialVector));
        t.is(decryptedData.name, content.name);
        t.is(decryptedData.dob, content.dob);
        t.is(decryptedData.phone, content.phone);
        t.is(decryptedData.address.ward_code, content.address.ward_code);
        t.is(decryptedData.address.region_code, content.address.region_code);
        t.is(decryptedData.address.district_code, content.address.district_code);
        t.is(decryptedData.address.street_address, content.address.street_address);
        t.is(decryptedData.national_id, content.national_id);
        t.is(decryptedData.branch_id, content.branch_id);
        t.is(decryptedData.account_id, content.account_id);
        await redis.del(redisConstantName);
    });
    it('can not access with invalid encrypted Data and encryptedKey', async t => {
        await redis.set(redisConstantName, JSON.stringify(DATA));
        const invalidEncryptedData = 'H54sRrzFl/BC0XxcZdrxT+GARRNLS3udlPA7iAuRAmGIqD4iEl5ej2AT7XaVV5FQA73FComXQr9Enn7EX+P2XTe7MTMuMJURPERealDQMDHREqTKK/gD2cppFJUiEb9OqhpWpGkzonMJdX93cbR7hxw+FJ4PprQgz0ARTsytLre1ENM5NF3GZlLCEjVDaqL5d+X8e1hzU6Ivrrf6cnDCbipvA7lJCedIdpp9x8p9hWOwZvmnuWcrSmwEz4MBUy8IMRd5BJRyv8gtFAxPEzusOUIKRUDw3ZcD01HaQt+PkES4rzWoxNvGbBYXr/KhOJnOzXnF6die3NJRuj/J3JMObw3YeslXDP2/aiehQ0xxSj9Qnm4hfgjqU3dtRxpGyEQUa/4KzftxJ6baQqIEMMv5jg0zjyLTPK6e1MwY2Imo9BCNsZXq83ARCggUDUPPHtye+zvLOUjEdW3UOUZx3kLwmw=='; //eslint-disable-line
        const invalidEncryptedKey = 'ReShJv1LuHR0zfsblTic0Vov+dEwwVvOCKvD9auXeKkuD2z1X6NV4GJeZsNCBcLZ1J/rJCpfSWWEqgzdlabrcSG4z0bZCGzSofBlku60ZzT/mqxPTNj1SSlO6BLA5xApHnmHL6jQV06+RydlK7VTBnNM+ocY6836chdgxC1cxASbPpVxBMM9AeRtyDlLS76zqD/m1ZHnA5LGlS9mD6I48bWpI767RRgr6euRpJ4idrkuFexz60/PQfSEff/vmXAtYerpOeEGjBwOB7moMQ8mpj6N4TbxwPG77iU7ji+knQz4S/se0kiYwC4VQkmvmW/BUepJHwnf9c2sjxPR7HtBSw=='; //eslint-disable-line
        const content = { encryptedData: invalidEncryptedData, encryptedKey: invalidEncryptedKey };
        const response = await request(app)
            .post(URI)
            .set(headers)
            .send(content)
            .expect(401)
            .then(res => res.body);
        t.is(response.name, 'AuthenticationError');
        await redis.del(redisConstantName);
    });
});
