import sequelizeFixtures from 'sequelize-fixtures';
import { dbHelpers } from 'firis-service-sdk-test2';
import path from 'path';
import db from '../src/models';


export function loadFixtures() {
    return dbHelpers.syncDB({ force: true, db })
        .then(() => sequelizeFixtures.loadFile(
            `${path.resolve(__dirname, '../')}/seeds/*.json`,
            db)
        );
}

/**
 * getAllElements - Gives you one of the fixture elements of a given type
 *
 * @param {model} - Model you want to get elements from
 * @return {Array}  - All fixture elements form db
 */
export function getAllElements(model) {
    return db[model].findAll()
        .then(objects => objects.map(object => object.toJSON()));
}
