import _ from 'lodash';
import db from '../models';
import { defaultPassword } from '../components/constants';
import AccountService from './account.service';

export default class User {
    static async list(query) {
        const whereCondition = !_.isEmpty(query) ? { type: query.type } : undefined;
        const user = await db.User.findAll({
            where: whereCondition
        });
        return user;
    }

    static async createAccount(user) {
        const account = await AccountService.create({
            username: user.national_id,
            password: defaultPassword,
            role: user.type,
            userId: user.id
        });
        return { ...account };
    }

    static async create(data) {
        const user = await db.User.create({ ...data });
        let account = {};
        try {
            account = await this.createAccount(user);
        } catch (err) {
            throw new Error(err);
        }
        return { ...user.dataValues, account: { ...account } };
    }

    static async update(data) {
        const user = await db.User.findOne({
            where: {
                id: data.id
            }
        });
        if (!user) throw new Error('Cannot find User');
        const updatedStaff = await user.updateAttributes({ ...data });
        return { ...updatedStaff.dataValues };
    }

    static async destroy({ id }) {
        const count = await db.User.destroy({ where: { id } });
        return { count };
    }
}

