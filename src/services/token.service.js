import jwt from 'jsonwebtoken';
import config from '../config';

export default {
    signAccessToken: (payload) => jwt.sign(
            { ...payload, tokenType: 'access_token' },
            config.jwtSecrect,
        )
};
