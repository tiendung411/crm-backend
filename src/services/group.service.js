import db from '../models';
import { ResourcesNotFound } from '../components/error';

export default class GroupService {
    static async retrieve({ id }) {
        const group = await db.Group.findOne({
            include: [{
                model: db.User,
                as: 'user'
            }],
            where: { id }
        });
        return { ...group.dataValues };
    }

    static async create(data) {
        const group = await db.Group.create({ name: data.name });
        return { ...group.dataValues };
    }

    static async update(data) {
        const group = await db.Group.findOne({
            where: {
                id: data.id
            }
        });
        if (!group) throw new ResourcesNotFound('Group');
        const updatedGroup = group.updateAttributes({ ...data });
        return { ...updatedGroup.dataValues };
    }
    static async list(data) {
        const whereCondition = data.type ? { where: { type: data.type } } : undefined;
        const group = await db.Group.findAll({
            include: [{
                model: db.User,
                as: 'user'
            }],
            ...whereCondition
        });
        if (!group) throw new ResourcesNotFound('Group');
        return group;
    }
}
