import db from '../models';
import { ResourcesNotFound } from '../components/error';

export default class Status {
    static async list() {
        const category = await db.Status.findAll();
        return category;
    }

    static async create(data) {
        const status = await db.Status.create({
            name: data.name
        });
        return { ...status };
    }

    static async update(data) {
        const status = await db.Status.findOne({
            where: {
                id: data.id
            }
        });
        if (!status) throw new ResourcesNotFound('Status');
        const updatedStatus = status.updateAttributes({ ...data });
        return { ...updatedStatus.dataValues };
    }

}

