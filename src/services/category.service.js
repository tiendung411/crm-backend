import db from '../models';

export default class Category {
    static async list() {
        const category = await db.Category.findAll();
        return category;
    }

    static async create(data) {
        const category = await db.Category.create({
            name: data.name
        });
        return { ...category };
    }

    static async update(data) {
        const category = await db.Category.findOne({
            where: {
                id: data.id
            }
        });
        if (!category) throw new Error('Cannot find User');
        const updatedCategory = category.updateAttributes({ ...data });
        return { ...updatedCategory.dataValues };
    }
}
