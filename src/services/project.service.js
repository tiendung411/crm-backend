/* eslint-disable no-param-reassign */
import _ from 'lodash';
import db from '../models';
import { ResourcesNotFound } from '../components/error';

export default class ProjectServer {
    static async getSource(project) {
        const source = await db.User.findOne({ where: { id: project.source } });
        return source ? source.toJSON() : null;
    }
    static async getSaleMan(project) {
        const data = await db.User.findOne({ where: { id: project.sale_man } });
        return data ? data.toJSON() : null;
    }
    static async getEngineer(project) {
        const data = await db.User.findOne({ where: { id: project.engineer } });
        return data ? data.toJSON() : null;
    }
    static async getStatus(project) {
        const data = await db.Status.findOne({ where: { id: project.status } });
        return data ? data.toJSON() : null;
    }
    static async getCategory(project) {
        const data = await db.Category.findOne({ where: { id: project.category } });
        return data ? data.toJSON() : null;
    }
    static async getGroupId(query) {
        const { userData } = query;
        const group = await db.Group.findOne({
            include: [
                {
                    model: db.User,
                    as: 'user',
                    where: { id: userData.userId }
                }
            ]
        });
        const { id } = group.toJSON();
        return id;
    }

    static async list(query) {
        const { userData } = query;
        let whereCondition;
        if (userData.role === 'saleman') {
            whereCondition = { group: await this.getGroupId(query) };
        }
        if (userData.role === 'marketer') {
            whereCondition = { source: userData.userId };
        }
        if (userData.role === 'engineer') {
            whereCondition = { engineer: userData.userId };
        }
        let projects = await db.Project.findAll({
            include: [
                {
                    model: db.Group,
                    as: 'project_group'
                }
            ],
            where: { ...whereCondition }
        });
        if (projects) {
            projects = projects.map(project => project.toJSON());
            for (let i = 0; i < projects.length; i++) {
                projects[i].project_source = await this.getSource(projects[i]);
                projects[i].project_saleman = await this.getSaleMan(projects[i]);
                projects[i].project_engineer = await this.getEngineer(projects[i]);
                projects[i].status = await this.getStatus(projects[i]);
                projects[i].category = await this.getCategory(projects[i]);
            }
        }
        return projects;
    }
    static async retrieve({ id }) {
        const project = await db.Project.findOne({
            include: [
                {
                    model: db.User,
                    as: 'project_saleman'
                },
                {
                    model: db.User,
                    as: 'project_source'
                },
                {
                    model: db.User,
                    as: 'project_engineer'
                },
                {
                    model: db.Status,
                    as: 'project_status'
                },
                {
                    model: db.Category,
                    as: 'project_category'
                }
            ],
            where: { id }
        });
        return { ...project.dataValues };
    }

    static async create(data) {
        const project = await db.Project.create({
            source: data.userData.userId,
            ...data.createInfo
        });
        return { ...project.dataValues };
    }

    static updateByAllowField(data, originData, allowedField) {
        const newUpdateData = _.cloneDeep(originData);
        allowedField.forEach(key => {
            newUpdateData[key] = data[key];
        });
        return newUpdateData;
    }

    static async update(data) {
        const { userData } = data;
        const project = await db.Project.findOne({
            where: {
                id: data.id
            }
        });
        if (!project) throw new ResourcesNotFound('Project');
        let updated = {};
        if (userData.role === 'saleman') {
            updated = this.updateByAllowField(
                data.updateInfo,
                project.toJSON(),
                ['status', 'note', 'engineer', 'category', 'sale_man']
            );
        }
        if (userData.role === 'marketer' && userData.userId !== project.toJSON().source) {
            if (data.updateInfo.sale_man) {
                throw new Error('Marketer can not update sale_man');
            }
            updated = this.updateByAllowField(
                data.updateInfo,
                project.toJSON(),
                ['name', 'mobile_phone', 'address', 'category', 'group']
            );
        }
        if (userData.role === 'admin') {
            updated = { ...data.updateInfo };
        }
        const updatedProject = await project.update(updated);
        return { ...updatedProject.dataValues };
    }

    static async destroy({ id }) {
        const count = await db.Project.destroy({ where: { id } });
        return { count };
    }
}
