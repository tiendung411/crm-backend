import md5 from 'md5';
import db from '../models';
import TokenService from './token.service';
import { AuthenticationError } from '../components/error';

export default class Account {
    static async login({ username, password }) {
        const hashedPassword = md5(password);
        const user = await db.User.findOne({
            include: [
                {
                    model: db.Account,
                    as: 'account',
                    where: {
                        username,
                        password: hashedPassword
                    },
                    required: true
                }
            ]
        });
        if (!user) throw new AuthenticationError('Username or Password is invalid');
        const token = TokenService.signAccessToken({
            id: user.account.id,
            role: user.account.role,
            accountId: user.account.id,
            userId: user.id
        });
        return { ...user.toJSON(), username, role: user.account.role, token };
    }

    static async create({ username, password, role, userId }) {
        const hashedPassword = md5(password);
        const account = await db.Account.create({
            username,
            password: hashedPassword,
            user_id: userId,
            role
        });
        return { ...account.dataValues };
    }
}

