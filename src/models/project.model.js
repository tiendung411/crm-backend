/**
* Example model - create and export the database model for the example
* including all assosiations and classmethods assiciated with this model.
* @memberof  module:models/Example
* @param  {Object} sequelize description
* @param  {Object} DataTypes description
*/

export default function (sequelize, DataTypes) {
    const Project = sequelize.define('project', {
        id: {
            type: DataTypes.UUID,
            allowNull: false,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        mobile_phone: {
            type: DataTypes.STRING,
            allowNull: false
        },
        address: {
            type: DataTypes.TEXT
        },
        category: {
            type: DataTypes.UUID
        },
        note: {
            type: DataTypes.TEXT
        },
        sale_man: {
            type: DataTypes.UUID
        },
        status: {
            type: DataTypes.UUID
        },
        engineer: {
            type: DataTypes.UUID
        },
        source: {
            type: DataTypes.UUID,
            allowNull: false
        },
        group: {
            type: DataTypes.UUID,
            allowNull: false
        },
        created_at: {
            type: DataTypes.DATE,
            allowNull: false
        },
        updated_at: {
            type: DataTypes.DATE,
            allowNull: false
        }
    }, {
        tableName: 'project',
        underscored: true
    });

    Project.associate = (models) => {
        Project.Group = Project.belongsTo(
            models.Group,
            {
                as: 'project_group',
                foreignKey: 'group',
                onDelete: 'cascade',
                onUpdate: 'cascade'
            }
        );

        Project.Source = Project.belongsTo(
            models.User,
            {
                as: 'project_source',
                foreignKey: 'source',
                onDelete: 'cascade',
                onUpdate: 'cascade'
            }
        );
    };


    return Project;
}
