/**
* Example model - create and export the database model for the example
* including all assosiations and classmethods assiciated with this model.
* @memberof  module:models/Example
* @param  {Object} sequelize description
* @param  {Object} DataTypes description
*/

export default function (sequelize, DataTypes) {
    const Group = sequelize.define('group', {
        id: {
            type: DataTypes.UUID,
            allowNull: false,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true
        },
        name: {
            type: DataTypes.STRING,
            unique: true
        },
        type: {
            type: DataTypes.STRING
        },
        created_at: {
            type: DataTypes.DATE,
            allowNull: false
        },
        updated_at: {
            type: DataTypes.DATE,
            allowNull: false
        }
    }, {
        tableName: 'group',
        underscored: true
    });

    Group.associate = (models) => {
        Group.User = Group.hasMany(
            models.User,
            {
                as: 'user',
                foreignKey: 'group_id',
                onDelete: 'cascade',
                onUpdate: 'cascade'
            }
        );
    };

    return Group;
}
