/**
* Example model - create and export the database model for the example
* including all assosiations and classmethods assiciated with this model.
* @memberof  module:models/Example
* @param  {Object} sequelize description
* @param  {Object} DataTypes description
*/

export default function (sequelize, DataTypes) {
    const User = sequelize.define('user', {
        id: {
            type: DataTypes.UUID,
            allowNull: false,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true
        },
        name: {
            type: DataTypes.STRING
        },
        mobile_phone: {
            type: DataTypes.STRING
        },
        address: {
            type: DataTypes.TEXT
        },
        national_id: {
            type: DataTypes.STRING,
            unique: true
        },
        group_id: {
            type: DataTypes.UUID
        },
        dob: {
            type: DataTypes.DATEONLY
        },
        type: {
            type: DataTypes.STRING
        },
        created_at: {
            type: DataTypes.DATE,
            allowNull: false
        },
        updated_at: {
            type: DataTypes.DATE,
            allowNull: false
        }
    }, {
        tableName: 'user',
        underscored: true
    });

    User.associate = (models) => {
        User.Account = User.hasOne(
            models.Account,
            {
                as: 'account',
                foreignKey: 'user_id',
                onDelete: 'cascade',
                onUpdate: 'cascade'
            }
        );
    };

    return User;
}
