/**
 * Main config file used throughout the application
 * @module config
 * @type {object}
 * @property {integer} port - The port to expose to application to.
 * @property {string} pgUrl - The url used by sequlize to connect to postgres.
 * @property {string} nodeEnv - The environment to run the server in
 * @property {string} sentry - Sentry DSN

 */
const config = {
    port: process.env.PORT || 9100,
    mysqlHost: process.env.MYSQL_HOST || 'localhost',
    mysqlPort: process.env.MYSQL_PORT || 3306,
    mysqlUser: process.env.MYSQL_USER || 'root',
    mysqlPassword: process.env.MYSQL_PASSWORD || 'password',
    mysqlDB: process.env.MYSQL_DB || 'crm',
    nodeEnv: process.env.NODE_ENV || 'development',
    jwtSecrect: process.env.JWT_SECRECT || 'crm'
};
export default config;
