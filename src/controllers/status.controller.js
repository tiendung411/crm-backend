import StatusService from '../services/status.service';

export const list = async (req, res, next) => {
    try {
        const data = {
            ...req.query
        };
        const rs = await StatusService.list(data);
        res.send(rs);
    } catch (err) { next(err); }
};


export const create = async (req, res, next) => {
    try {
        const rs = await StatusService.create(req.body);
        res.send(rs);
    } catch (err) { next(err); }
};

export const update = async (req, res, next) => {
    try {
        const data = {
            id: req.params.id,
            ...req.body
        };
        const rs = await StatusService.update(data);
        res.send(rs);
    } catch (err) { next(err); }
};
