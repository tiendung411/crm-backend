import UserService from '../services/user.service';

export const list = async (req, res, next) => {
    try {
        const data = {
            ...req.query
        };
        const rs = await UserService.list(data);
        res.send(rs);
    } catch (err) { next(err); }
};


export const create = async (req, res, next) => {
    try {
        const rs = await UserService.create(req.body);
        res.send(rs);
    } catch (err) { next(err); }
};

export const update = async (req, res, next) => {
    try {
        const data = {
            id: req.params.id,
            ...req.body
        };
        const rs = await UserService.update(data);
        res.send(rs);
    } catch (err) { next(err); }
};

export const destroy = async (req, res, next) => {
    try {
        const data = {
            id: req.params.id
        };
        const rs = await UserService.destroy(data);
        res.send(rs);
    } catch (err) { next(err); }
};
