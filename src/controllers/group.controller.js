import GroupService from '../services/group.service';

export const retrieve = async (req, res, next) => {
    try {
        const data = {
            id: req.params.id
        };
        const rs = await GroupService.retrieve(data);
        res.send(rs);
    } catch (err) { next(err); }
};

export const create = async (req, res, next) => {
    try {
        const rs = await GroupService.create(req.body);
        res.send(rs);
    } catch (err) { next(err); }
};

export const update = async (req, res, next) => {
    try {
        const data = {
            id: req.params.id,
            ...req.body
        };
        const rs = await GroupService.update(data);
        res.send(rs);
    } catch (err) { next(err); }
};

export const list = async (req, res, next) => {
    try {
        const data = {
            ...req.query
        };
        const rs = await GroupService.list(data);
        res.send(rs);
    } catch (err) { next(err); }
};
