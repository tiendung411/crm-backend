import AccountService from '../services/account.service';

export const login = async (req, res, next) => {
    try {
        const rs = await AccountService.login(req.body);
        res.send(rs);
    } catch (err) { next(err); }
};

export const create = async (req, res, next) => {
    try {
        const rs = await AccountService.create(req.body);
        res.send(rs);
    } catch (err) { next(err); }
};
