import ProjectService from '../services/project.service';


export const list = async (req, res, next) => {
    try {
        const data = {
            userData: res.locals.user,
            ...req.query
        };
        const rs = await ProjectService.list(data);
        res.send(rs);
    } catch (err) { next(err); }
};

export const retrieve = async (req, res, next) => {
    try {
        const data = {
            id: req.params.id
        };
        const rs = await ProjectService.retrieve(data);
        res.send(rs);
    } catch (err) { next(err); }
};

export const create = async (req, res, next) => {
    try {
        const data = {
            userData: res.locals.user,
            createInfo: req.body
        };
        const rs = await ProjectService.create(data);
        res.send(rs);
    } catch (err) { next(err); }
};

export const update = async (req, res, next) => {
    try {
        const data = {
            userData: res.locals.user,
            id: req.params.id,
            updateInfo: req.body
        };
        const rs = await ProjectService.update(data);
        res.send(rs);
    } catch (err) { next(err); }
};

export const destroy = async (req, res, next) => {
    try {
        const data = {
            id: req.params.id
        };
        const rs = await ProjectService.destroy(data);
        res.send(rs);
    } catch (err) { next(err); }
};
