import CategoryService from '../services/category.service';

export const list = async (req, res, next) => {
    try {
        const data = {
            ...req.query
        };
        const rs = await CategoryService.list(data);
        res.send(rs);
    } catch (err) { next(err); }
};


export const create = async (req, res, next) => {
    try {
        const rs = await CategoryService.create(req.body);
        res.send(rs);
    } catch (err) { next(err); }
};

export const update = async (req, res, next) => {
    try {
        const data = {
            id: req.params.id,
            ...req.body
        };
        const rs = await CategoryService.update(data);
        res.send(rs);
    } catch (err) { next(err); }
};
