import jwt from 'jsonwebtoken';
import sequelize from 'sequelize';
import config from '../config';
import { AuthenticationError, DataValidationError, BusinessError } from '../components/error';

export function verifyJWT() {
    return (req, res, next) => {
        const token = req.body.token || req.query.token || req.headers['x-access-token'];
        jwt.verify(token, config.jwtSecrect, (err, decoded) => {
            if (err) {
                throw new AuthenticationError(err.message);
            }
            if (!decoded) {
                throw new AuthenticationError('Invalid Data for authorization');
            }
            res.locals.user = decoded; //eslint-disable-line
            next();
        });
    };
}

export function permitRole(allowed) {
    return (req, res, next) => {
        const userData = res.locals.user;
        const isAllowed = allowed.indexOf(userData.role) > -1;
        if (!isAllowed) {
            throw new Error('Your Role is not allowed');
        }
        next();
    };
}

// eslint-disable-next-line no-unused-vars
export function handleError(err, req, res, next) {
    let formartedError = {};
    if (err instanceof sequelize.ValidationError) {
        formartedError = new DataValidationError(err, sequelize);
        formartedError = {
            status: formartedError.status,
            name: formartedError.name,
            message: formartedError.message,
            payload: formartedError.payload
        };
    } else if (err instanceof BusinessError) {
        formartedError = {
            status: err.status,
            name: err.name,
            message: err.message
        };
    } else {
        throw new Error(err);
    }
    res.status(formartedError.status).send(formartedError);
}
