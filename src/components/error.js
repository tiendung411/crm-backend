export class ApplicationError extends Error {
    constructor(message, status) {
        super();

        Error.captureStackTrace(this, this.constructor);

        this.name = this.constructor.name;

        this.message = message ||
            'Something went wrong. Please try again.';

        this.status = status || 500;
    }
}

export class BusinessError extends ApplicationError {
    constructor(message, status) {
        super();

        Error.captureStackTrace(this, this.constructor);

        this.name = this.constructor.name;

        this.message = message ||
            'Business Error';

        this.status = status || 400;
    }
}


export class AuthenticationError extends BusinessError {
    constructor(message) {
        super(message, 401);
    }
}

export class ResourcesNotFound extends BusinessError {
    constructor(message) {
        super(`Cannot find ${message}`, 404);
    }
}

export class DataValidationError extends ApplicationError {
    constructor(err, sequelize) {
        super(err.message, 400);
        if (err instanceof sequelize.ValidationError) {
            this.payload = err.errors.reduce((finalErrors, itemError) => ({
                ...finalErrors,
                [itemError.path]: {
                    message: itemError.message,
                    type: itemError.validatorKey,
                    context: {
                        value: itemError.value,
                        agrs: itemError.validatorArgs
                    }
                }
            }), {});
        }
    }
}
