import express from 'express';
import { permitRole } from '../components/middleware';
import { list, create, update, destroy } from '../controllers/project.controller';

const router = express.Router();

router.get(
    '/',
    permitRole(['admin', 'saleman', 'marketer', 'engineer']),
    list
);


router.post(
    '/',
    permitRole(['marketer', 'admin']),
    create
);

router.put(
    '/:id',
    permitRole(['admin', 'saleman', 'marketer']),
    update
);

router.delete(
    '/:id',
    permitRole(['admin']),
    destroy
);


export default router;
