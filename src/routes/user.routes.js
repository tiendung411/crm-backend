import express from 'express';
import { permitRole } from '../components/middleware';
import { create, update, destroy, list } from '../controllers/user.controller';

const router = express.Router();

router.get(
    '/',
    permitRole(['admin', 'saleman']),
    list
);


router.post(
    '/',
    permitRole(['admin']),
    create
);

router.put(
    '/:id',
    permitRole(['admin']),
    update
);

router.delete(
    '/:id',
    permitRole(['admin']),
    destroy
);


export default router;
