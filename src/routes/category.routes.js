import express from 'express';
import { permitRole } from '../components/middleware';
import { create, update, list } from '../controllers/category.controller';

const router = express.Router();

router.get(
    '/',
    permitRole(['admin', 'saleman', 'marketer', 'engineer']),
    list
);

router.post(
    '/',
    permitRole(['admin', 'saleman', 'marketer', 'engineer']),
    create
);

router.put(
    '/:id',
    permitRole(['admin', 'saleman', 'marketer', 'engineer']),
    update
);


export default router;
