import express from 'express';
import { permitRole, verifyJWT } from '../components/middleware';
import { login, create } from '../controllers/account.controller';

const router = express.Router();

router.post(
    '/login',
    login
);

router.post(
    '/',
    verifyJWT(),
    permitRole(['admin']),
    create
);

export default router;
