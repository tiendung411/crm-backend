import express from 'express';
import { permitRole } from '../components/middleware';
import { create, update, retrieve, list } from '../controllers/group.controller';

const router = express.Router();

router.get(
    '/',
    permitRole(['admin', 'saleman', 'marketer', 'engineer']),
    list
);

router.get(
    '/:id',
    permitRole(['admin','saleman', 'marketer', 'engineer']),
    retrieve
);

router.post(
    '/',
    permitRole(['admin']),
    create
);

router.put(
    '/:id',
    permitRole(['admin']),
    update
);


export default router;
