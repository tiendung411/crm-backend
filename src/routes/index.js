import express from 'express';
import { verifyJWT, handleError } from '../components/middleware';
import account from './account.routes';
import user from './user.routes';
import status from './status.routes';
import group from './group.routes';
import project from './project.routes';
import category from './category.routes';


const router = express.Router();

router.use('/account', account);
router.use('/user', verifyJWT(), user);
router.use('/group', verifyJWT(), group);
router.use('/status', verifyJWT(), status);
router.use('/project', verifyJWT(), project);
router.use('/category', verifyJWT(), category);
router.use(handleError);

export default router;
