import express from 'express';
import { permitRole } from '../components/middleware';
import { create, update, list } from '../controllers/status.controller';

const router = express.Router();

router.get(
    '/',
    permitRole(['admin', 'saleman', 'marketer', 'engineer']),
    list
);

router.post(
    '/',
    permitRole(['admin']),
    create
);

router.put(
    '/:id',
    permitRole(['admin']),
    update
);


export default router;
