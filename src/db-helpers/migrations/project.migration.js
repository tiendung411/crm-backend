
// eslint-disable-next-line import/prefer-default-export
export function up(queryInterface, Sequelize) {
    return queryInterface.createTable(
        'project',
        {
            id: {
                type: Sequelize.UUID,
                allowNull: false,
                defaultValue: Sequelize.UUIDV4,
                primaryKey: true
            },
            name: {
                type: Sequelize.STRING,
                allowNull: false
            },
            mobile_phone: {
                type: Sequelize.STRING,
                allowNull: false
            },
            address: {
                type: Sequelize.TEXT
            },
            category: {
                type: Sequelize.UUID
            },
            note: {
                type: Sequelize.TEXT
            },
            sale_man: {
                type: Sequelize.UUID
            },
            status: {
                type: Sequelize.UUID
            },
            engineer: {
                type: Sequelize.UUID
            },
            source: {
                type: Sequelize.UUID,
                allowNull: false
            },
            group: {
                type: Sequelize.UUID,
                allowNull: false
            },
            created_at: {
                type: Sequelize.DATE,
                allowNull: false
            },
            updated_at: {
                type: Sequelize.DATE,
                allowNull: false
            }
        }
    ).then(() => queryInterface.addConstraint('project', ['group'], {
        type: 'FOREIGN KEY',
        name: 'project_group_id_fkey',
        references: {
            table: 'group',
            field: 'id'
        },
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
    }));
}
