
// eslint-disable-next-line import/prefer-default-export
export function up(queryInterface, Sequelize) {
    return queryInterface.createTable(
        'group',
        {
            id: {
                type: Sequelize.UUID,
                allowNull: false,
                defaultValue: Sequelize.UUIDV4,
                primaryKey: true
            },
            name: {
                type: Sequelize.STRING,
                unique: true
            },
            type: {
                type: Sequelize.ENUM('admin', 'engineer', 'saleman', 'marketer')
            },
            created_at: {
                type: Sequelize.DATE,
                allowNull: false
            },
            updated_at: {
                type: Sequelize.DATE,
                allowNull: false
            }
        }
    );
}
