
// eslint-disable-next-line import/prefer-default-export
export function up(queryInterface, Sequelize) {
    return queryInterface.createTable(
        'user',
        {
            id: {
                type: Sequelize.UUID,
                allowNull: false,
                defaultValue: Sequelize.UUIDV4,
                primaryKey: true
            },
            name: {
                type: Sequelize.STRING
            },
            mobile_phone: {
                type: Sequelize.STRING
            },
            address: {
                type: Sequelize.TEXT
            },
            national_id: {
                type: Sequelize.STRING,
                unique: true
            },
            group_id: {
                type: Sequelize.UUID
            },
            dob: {
                type: Sequelize.DATEONLY
            },
            type: {
                type: Sequelize.STRING
            },
            created_at: {
                type: Sequelize.DATE,
                allowNull: false
            },
            updated_at: {
                type: Sequelize.DATE,
                allowNull: false
            }
        }
    ).then(() => queryInterface.addConstraint('account', ['user_id'], {
        type: 'FOREIGN KEY',
        name: 'user_account_id_fkey',
        references: {
            table: 'user',
            field: 'id'
        },
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
    })).then(() => queryInterface.addConstraint('user', ['group_id'], {
        type: 'FOREIGN KEY',
        name: 'user_group_id_fkey',
        references: {
            table: 'group',
            field: 'id'
        },
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
    }))
    .then(() => queryInterface.addConstraint('project', ['source'], {
        type: 'FOREIGN KEY',
        name: 'user_project_source_id_fkey',
        references: {
            table: 'user',
            field: 'id'
        },
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
    }));
}
