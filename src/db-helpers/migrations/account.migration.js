
// eslint-disable-next-line import/prefer-default-export
export function up(queryInterface, Sequelize) {
    return queryInterface.createTable(
        'account',
        {
            id: {
                type: Sequelize.UUID,
                allowNull: false,
                defaultValue: Sequelize.UUIDV4,
                primaryKey: true
            },
            username: {
                type: Sequelize.STRING,
                unique: true
            },
            password: {
                type: Sequelize.STRING
            },
            user_id: {
                type: Sequelize.UUID
            },
            role: {
                type: Sequelize.ENUM('admin', 'engineer', 'saleman', 'marketer')
            },
            created_at: {
                type: Sequelize.DATE,
                allowNull: false
            },
            updated_at: {
                type: Sequelize.DATE,
                allowNull: false
            }
        }
    );
}
