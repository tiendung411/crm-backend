
// eslint-disable-next-line import/prefer-default-export
export function up(queryInterface, Sequelize) {
    return queryInterface.createTable(
        'status',
        {
            id: {
                type: Sequelize.UUID,
                allowNull: false,
                defaultValue: Sequelize.UUIDV4,
                primaryKey: true
            },
            name: {
                type: Sequelize.STRING
            },
            meeting_date: {
                type: Sequelize.DATEONLY
            },
            created_at: {
                type: Sequelize.DATE,
                allowNull: false
            },
            updated_at: {
                type: Sequelize.DATE,
                allowNull: false
            }
        }
    );
}
