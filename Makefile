RELEASE_VERSION := $(shell ./version.sh)
ifeq ($(RELEASE_VERSION),)
RELEASE_VERSION := 0.0.1
endif
NAME            := docker-registry.firis.com.vn:5000/user-service
TAG             := $$(git rev-parse --short HEAD)
IMG             := ${NAME}:${RELEASE_VERSION}_${TAG}
LATEST          := ${NAME}:${RELEASE_VERSION}_${TAG}

build:
	@docker build -t ${IMG} .
	@docker tag ${IMG} ${LATEST}

push:
	@docker push ${NAME}

login:
	@docker log -u ${DOCKER_USER} -p ${DOCKER_PASS}

tag:
	sed -i -e "s/version:.*/version: $(RELEASE_VERSION)/" charts/user-service/Chart.yaml
	sed -i -e "s|repository: .*|repository: $(NAME)|" charts/user-service/values.yaml
	sed -i -e "s/tag: .*/tag: $(RELEASE_VERSION)_$(TAG)/" charts/user-service/values.yaml
